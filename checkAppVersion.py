import os
import tkinter as tk
from tkinter import ttk
import directories_to_search


def check_string_in_file_name(file_path, search_string):
    for file in os.listdir(file_path):
        if os.path.isfile(os.path.join(file_path, file)) and search_string in file:
            return os.path.join("", file)
    return None


def check_string_in_folder_name(folder_path, search_string):
    for folder in os.listdir(folder_path):
        if os.path.isdir(os.path.join(folder_path, folder)) and search_string in folder:
            return os.path.join("", folder)
    return None


def submit_button_click():
    paths = None
    result_text = ""
    if selected_option.get() == "City Index":
        paths = directories_to_search.city_index_paths
    elif selected_option.get() == "Forex":
        paths = directories_to_search.forex_paths
    elif selected_option.get() == "Partners":
        paths = directories_to_search.partners_paths
    elif selected_option.get() == "Market Info":
        paths = directories_to_search.market_info_paths
    elif selected_option.get() == "Standalone Charts":
        paths = directories_to_search.standalone_charts_paths
    elif selected_option.get() == "Settings":
        paths = directories_to_search.settings_paths
    elif selected_option.get() == "Trading Central Widgets":
        paths = directories_to_search.trading_central_widgets_paths
    elif selected_option.get() == "Branded Login - City Index":
        paths = directories_to_search.branded_login_ci_paths
    elif selected_option.get() == "Branded Login - Forex":
        paths = directories_to_search.branded_login_forex_paths
    elif selected_option.get() == "Branded Login - WHS":
        paths = directories_to_search.branded_login_whs_paths
    elif selected_option.get() == "Branded Login - Tradefair":
        paths = directories_to_search.branded_login_tradefair_paths
    elif selected_option.get() == "Branded Login - KQ":
        paths = directories_to_search.branded_login_kq_paths
    elif selected_option.get() == "Branded Login - JSzhanghu":
        paths = directories_to_search.branded_login_jszhanghu_paths

    search_string = entry.get()
    for path in paths:
        file_result = check_string_in_file_name(path, search_string)
        if file_result is not None:
            result_text = result_text + f"{path} - CURRENT FILE - {file_result}"
        else:
            result_text = result_text + f"{path} - File not found!"

        folder_result = check_string_in_folder_name(path, search_string)
        if folder_result is not None:
            result_text = result_text + f" - VERSION FOLDER - {folder_result}\n"
        else:
            result_text = result_text + f" - Folder not found!\n"

    result_label.config(text=f"{result_text}")


def clear_button_click():
    entry.delete(0, tk.END)


# create GUI window
window = tk.Tk()
window.geometry("1200x800")
window.title("File Search App")

# create search entry
entry_label = tk.Label(text="Enter version number:")
entry_label.pack()
entry = tk.Entry(width=20)
entry.pack()

# create clear button
clear_button = tk.Button(text="Clear", command=clear_button_click)
clear_button.pack()

# create option menu
option_label = tk.Label(text="Choose brand or project:")
option_label.pack()
selected_option = tk.StringVar()
option_menu = ttk.OptionMenu(window, selected_option, "City Index", "City Index", "Forex", "Partners",
                             "Market Info", "Standalone Charts", "Settings", "Trading Central Widgets",
                             "Branded Login - City Index", "Branded Login - Forex", "Branded Login - WHS",
                             "Branded Login - Tradefair", "Branded Login - KQ", "Branded Login - JSzhanghu")
option_menu.pack()

# create submit button
submit_button = tk.Button(text="Submit", command=submit_button_click)
submit_button.pack()

# create result label
result_label = tk.Label(text="")
result_label.pack()

# start GUI
window.mainloop()
