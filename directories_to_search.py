city_index_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\webtrader",
                    r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\webtrader",
                    r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\webtrader",
                    r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\webtrader",
                    r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\webtrader",
                    r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\webtrader",
                    r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\webtrader",
                    r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\webtrader"
                    ]

forex_paths = [r"\\inx-srv-webl48\Websites\webtrader.forex.com",
               r"\\inx-srv-webl49\Websites\webtrader.forex.com",
               r"\\inx-srv-webl78\Websites\webtrader.forex.com",
               r"\\inx-srv-webl79\Websites\webtrader.forex.com",
               r"\\rdb-srv-webl48\Websites\webtrader.forex.com",
               r"\\rdb-srv-webl49\Websites\webtrader.forex.com",
               r"\\rdb-srv-webl78\Websites\webtrader.forex.com",
               r"\\rdb-srv-webl79\Websites\webtrader.forex.com",
               r"\\inx-srv-webl48\Websites\webtrader.denglupingtai.com",
               r"\\inx-srv-webl49\Websites\webtrader.denglupingtai.com",
               r"\\inx-srv-webl78\Websites\webtrader.denglupingtai.com",
               r"\\inx-srv-webl79\Websites\webtrader.denglupingtai.com",
               r"\\rdb-srv-webl48\Websites\webtrader.denglupingtai.com",
               r"\\rdb-srv-webl49\Websites\webtrader.denglupingtai.com",
               r"\\rdb-srv-webl78\Websites\webtrader.denglupingtai.com",
               r"\\rdb-srv-webl79\Websites\webtrader.denglupingtai.com",
               r"\\inx-srv-webl48\Websites\webtrader.jszhanghao.com",
               r"\\inx-srv-webl49\Websites\webtrader.jszhanghao.com",
               r"\\inx-srv-webl78\Websites\webtrader.jszhanghao.com",
               r"\\inx-srv-webl79\Websites\webtrader.jszhanghao.com",
               r"\\rdb-srv-webl48\Websites\webtrader.jszhanghao.com",
               r"\\rdb-srv-webl49\Websites\webtrader.jszhanghao.com",
               r"\\rdb-srv-webl78\Websites\webtrader.jszhanghao.com",
               r"\\rdb-srv-webl79\Websites\webtrader.jszhanghao.com",
               r"\\inx-srv-webl48\Websites\webtrader.jszhanghu.com",
               r"\\inx-srv-webl49\Websites\webtrader.jszhanghu.com",
               r"\\inx-srv-webl78\Websites\webtrader.jszhanghu.com",
               r"\\inx-srv-webl79\Websites\webtrader.jszhanghu.com",
               r"\\rdb-srv-webl48\Websites\webtrader.jszhanghu.com",
               r"\\rdb-srv-webl49\Websites\webtrader.jszhanghu.com",
               r"\\rdb-srv-webl78\Websites\webtrader.jszhanghu.com",
               r"\\rdb-srv-webl79\Websites\webtrader.jszhanghu.com"
               ]

partners_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\allyinvest\webtrader",
                  r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\allyinvest\webtrader",
                  r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\allyinvest\webtrader",
                  r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\allyinvest\webtrader",
                  r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\allyinvest\webtrader",
                  r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\allyinvest\webtrader",
                  r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\allyinvest\webtrader",
                  r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\allyinvest\webtrader",
                  r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\tradefair\webtrader",
                  r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\tradefair\webtrader",
                  r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\tradefair\webtrader",
                  r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\tradefair\webtrader",
                  r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\tradefair\webtrader",
                  r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\tradefair\webtrader",
                  r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\tradefair\webtrader",
                  r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\tradefair\webtrader",
                  r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\whs\webtrader",
                  r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\whs\webtrader",
                  r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\whs\webtrader",
                  r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\whs\webtrader",
                  r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\whs\webtrader",
                  r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\whs\webtrader",
                  r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\whs\webtrader",
                  r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\whs\webtrader"
                  ]

market_info_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\marketinfo",
                     r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\marketinfo",
                     r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\marketinfo",
                     r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\marketinfo",
                     r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\marketinfo",
                     r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\marketinfo",
                     r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\marketinfo",
                     r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\marketinfo"
                     ]

standalone_charts_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\charts",
                           r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\charts",
                           r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\charts",
                           r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\charts",
                           r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\charts",
                           r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\charts",
                           r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\charts",
                           r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\charts"
                           ]

settings_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\settings",
                  r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\settings",
                  r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\settings",
                  r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\settings",
                  r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\settings",
                  r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\settings",
                  r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\settings",
                  r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\settings"
                  ]

trading_central_widgets_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\tcw",
                                 r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\tcw",
                                 r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\tcw",
                                 r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\tcw",
                                 r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\tcw",
                                 r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\tcw",
                                 r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\tcw",
                                 r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\tcw"
                                 ]

branded_login_ci_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\cityindex",
                          r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\cityindex",
                          r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\cityindex",
                          r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\cityindex",
                          r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\cityindex",
                          r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\cityindex",
                          r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\cityindex",
                          r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\cityindex"
                          ]

branded_login_forex_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\forex",
                             r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\forex",
                             r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\forex",
                             r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\forex",
                             r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\forex",
                             r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\forex",
                             r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\forex",
                             r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\forex"
                             ]

branded_login_whs_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\whs",
                           r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\whs",
                           r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\whs",
                           r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\whs",
                           r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\whs",
                           r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\whs",
                           r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\whs",
                           r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\whs"
                           ]

branded_login_tradefair_paths = [r"\\inx-srv-webl48\Websites\trade.loginandtrade.com\tradefair",
                                 r"\\inx-srv-webl49\Websites\trade.loginandtrade.com\tradefair",
                                 r"\\inx-srv-webl78\Websites\trade.loginandtrade.com\tradefair",
                                 r"\\inx-srv-webl79\Websites\trade.loginandtrade.com\tradefair",
                                 r"\\rdb-srv-webl48\Websites\trade.loginandtrade.com\tradefair",
                                 r"\\rdb-srv-webl49\Websites\trade.loginandtrade.com\tradefair",
                                 r"\\rdb-srv-webl78\Websites\trade.loginandtrade.com\tradefair",
                                 r"\\rdb-srv-webl79\Websites\trade.loginandtrade.com\tradefair"
                                 ]

branded_login_kq_paths = [r"\\inx-srv-webl48\Websites\webtrader.forex.com\KQ",
                          r"\\inx-srv-webl49\Websites\webtrader.forex.com\KQ",
                          r"\\inx-srv-webl78\Websites\webtrader.forex.com\KQ",
                          r"\\inx-srv-webl79\Websites\webtrader.forex.com\KQ",
                          r"\\rdb-srv-webl48\Websites\webtrader.forex.com\KQ",
                          r"\\rdb-srv-webl49\Websites\webtrader.forex.com\KQ",
                          r"\\rdb-srv-webl78\Websites\webtrader.forex.com\KQ",
                          r"\\rdb-srv-webl79\Websites\webtrader.forex.com\KQ"
                          ]

branded_login_jszhanghu_paths = [r"\\inx-srv-webl48\Websites\webtrader.jszhanghu.com\login",
                                 r"\\inx-srv-webl49\Websites\webtrader.jszhanghu.com\login",
                                 r"\\inx-srv-webl78\Websites\webtrader.jszhanghu.com\login",
                                 r"\\inx-srv-webl79\Websites\webtrader.jszhanghu.com\login",
                                 r"\\rdb-srv-webl48\Websites\webtrader.jszhanghu.com\login",
                                 r"\\rdb-srv-webl49\Websites\webtrader.jszhanghu.com\login",
                                 r"\\rdb-srv-webl78\Websites\webtrader.jszhanghu.com\login",
                                 r"\\rdb-srv-webl79\Websites\webtrader.jszhanghu.com\login"
                                 ]
